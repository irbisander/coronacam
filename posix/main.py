import asyncio
import json
import base64
import cv2
import numpy as np
import websockets
import smbus
import time
import os

class MLX90614():

    MLX90614_RAWIR1=0x04
    MLX90614_RAWIR2=0x05
    MLX90614_TA=0x06
    MLX90614_TOBJ1=0x07
    MLX90614_TOBJ2=0x08

    MLX90614_TOMAX=0x20
    MLX90614_TOMIN=0x21
    MLX90614_PWMCTRL=0x22
    MLX90614_TARANGE=0x23
    MLX90614_EMISS=0x24
    MLX90614_CONFIG=0x25
    MLX90614_ADDR=0x0E
    MLX90614_ID1=0x3C
    MLX90614_ID2=0x3D
    MLX90614_ID3=0x3E
    MLX90614_ID4=0x3F

    comm_retries = 5
    comm_sleep_amount = 0.1

    def __init__(self, address=0x5a, bus_num=1):
        self.bus_num = bus_num
        self.address = address
        self.bus = smbus.SMBus(bus=bus_num)

    def read_reg(self, reg_addr):
        err = None
        for i in range(self.comm_retries):
            try:
                return self.bus.read_word_data(self.address, reg_addr)
            except IOError as e:
                err = e
                #"Rate limiting" - sleeping to prevent problems with sensor
                #when requesting data too quickly
                time.sleep(self.comm_sleep_amount)
        #By this time, we made a couple requests and the sensor didn't respond
        #(judging by the fact we haven't returned from this function yet)
        #So let's just re-raise the last IOError we got
        raise err

    def data_to_temp(self, data):
        temp = ((data*0.02) - 273.15)+2*0.6
        return temp

    def get_amb_temp(self):
        data = self.read_reg(self.MLX90614_TA)
        return self.data_to_temp(data)

    def get_obj_temp(self):
        data = self.read_reg(self.MLX90614_TOBJ1)
        return self.data_to_temp(data)
    def get_obj2_temp(self):
        data = self.read_reg(self.MLX90614_TOBJ2)
        return self.data_to_temp(data)




if __name__ == "__main__":
    sensor = MLX90614()
    print(sensor.get_amb_temp())
    print(sensor.get_obj_temp())


thermometer_address = 0x5a

thermometer = MLX90614(thermometer_address)
font = cv2.FONT_HERSHEY_SIMPLEX

cap = cv2.VideoCapture(0)
delta = 0;
faceCascade = cv2.CascadeClassifier('/home/pi/Desktop/coronacam-master/assets/haar/haarcascade_frontalface_default.xml')
faceCascade2 = cv2.CascadeClassifier('/home/pi/Desktop/coronacam-master/assets/haar/haarcascade_frontalface_alt.xml')
faceCascade3 = cv2.CascadeClassifier('/home/pi/Desktop/coronacam-master/assets/haar/haarcascade_frontalface_alt2.xml')
faceCascade4 = cv2.CascadeClassifier('/home/pi/Desktop/coronacam-master/assets/haar/haarcascade_frontalface_alt_tree.xml')

async def getData(websocket, path):
    global delta;
    data = await websocket.recv();
    data = json.loads(data);
    if(data["action"]=="setT"):
                delta=data["editT"];
                
def getInfo(websocket):
    global thermometer,delta,cap
    # Capture frame-by-frame
    rret, img = cap.read()
            
    img = cv2.flip(img, 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces2 = faceCascade2.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces3 = faceCascade3.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces4 = faceCascade4.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    t=thermometer.get_obj_temp();
    for (x,y,w,h) in faces:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(0,255,255)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(t+delta), (x+5,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces2:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,0,255)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(2), (x+20,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces3:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,255,0)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(3), (x+35,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces4:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,255,0)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(4), (x+50,y-5), font, 1, color, 2)
    retval, buffer =  cv2.imencode('.jpg', img);
    jpg_as_text = base64.b64encode(buffer)
    base64_string = jpg_as_text.decode('utf-8')
    #print("frame ready");
    dt = json.dumps({"img": base64_string,"t":t+delta})
    return dt;



async def handler(websocket, path):
    global delta;
    while True:
        await websocket.send(getInfo(websocket));
            
            

    
start_server = websockets.serve( handler,"127.0.0.1", 5678,compression = None )
start_server2 = websockets.serve( getData,"127.0.0.1", 5677,compression = None )
asyncio.get_event_loop().run_until_complete(asyncio.gather(
    start_server,
    start_server2
))
asyncio.get_event_loop().run_forever()
