import os
import asyncio
import json
import base64
import cv2
import numpy as np
import websockets

import time


font = cv2.FONT_HERSHEY_SIMPLEX

cap = cv2.VideoCapture(0)
delta = 0;
faceCascade = cv2.CascadeClassifier('./assets/haar/haarcascade_frontalface_default.xml')
faceCascade2 = cv2.CascadeClassifier('./assets/haar/haarcascade_frontalface_alt.xml')
faceCascade3 = cv2.CascadeClassifier('./assets/haar/haarcascade_frontalface_alt2.xml')
faceCascade4 = cv2.CascadeClassifier('./assets/haar/haarcascade_frontalface_alt_tree.xml')

async def getData(websocket, path):
    global delta;
    data = await websocket.recv();
    data = json.loads(data);
    if(data["action"]=="setT"):
                delta=data["editT"];
                
def getInfo(websocket):
    global thermometer,delta,cap
    # Capture frame-by-frame
    rret, img = cap.read()
            
    img = cv2.flip(img, 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces2 = faceCascade2.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces3 = faceCascade3.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    faces4 = faceCascade4.detectMultiScale(
                    gray,     
                    scaleFactor=1.2,
                    minNeighbors=5,     
                    minSize=(20, 20)
                )
    t=0;
    for (x,y,w,h) in faces:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(0,255,255)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(t+delta), (x+5,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces2:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,0,255)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(2), (x+20,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces3:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,255,0)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(3), (x+35,y-5), font, 1, color, 2)
    for (x,y,w,h) in faces4:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]  
                color=(255,255,0)
                if(t<37):
                    color=(0,255,0)
                else:
                    color(255,0,0)
                cv2.putText(img, str(4), (x+50,y-5), font, 1, color, 2)
    retval, buffer =  cv2.imencode('.jpg', img);
    jpg_as_text = base64.b64encode(buffer)
    base64_string = jpg_as_text.decode('utf-8')
    #print("frame ready");
    dt = json.dumps({"img": base64_string,"t":t+delta})
    return dt;



async def handler(websocket, path):
    global delta;
    while True:
        await websocket.send(getInfo(websocket));
            
            

    
start_server = websockets.serve( handler,"127.0.0.1", 5678,compression = None )
start_server2 = websockets.serve( getData,"127.0.0.1", 5677,compression = None )
asyncio.get_event_loop().run_until_complete(asyncio.gather(
    start_server,
    start_server2
))
asyncio.get_event_loop().run_forever()
